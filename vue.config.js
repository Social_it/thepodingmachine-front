module.exports = {
  devServer: {
    disableHostCheck: true,
    proxy: "http://ec2-34-254-91-44.eu-west-1.compute.amazonaws.com:3000/"
  },
  pluginOptions: {
    i18n: {
      locale: "fr",
      fallbackLocale: "fr",
      localeDir: "locales",
      enableInSFC: false
    }
  },
  css: {
    loaderOptions: {
      sass: {
        prependData: `
          @import "@/assets/style/globals.scss";
        `
      }
    }
  }
};
