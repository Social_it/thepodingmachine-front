import Vue from "vue";
import Vuex from "vuex";
import { user } from "@/store/modules/user";
import { posts } from "@/store/modules/posts";
import { pods } from "@/store/modules/pods";
import { lists } from "@/store/modules/lists";
import { comments } from "@/store/modules/comments";
import { subscriptions } from "@/store/modules/subscriptions";

Vue.use(Vuex);

const store = {
  modules: {
    user,
    posts,
    pods,
    lists,
    comments,
    subscriptions
  }
};

export default new Vuex.Store(store);
