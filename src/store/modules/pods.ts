import { Module } from "vuex";
import { PodState } from "@/store/modules/types";
import { GetterTree, MutationTree, ActionTree } from "vuex";
import axios from "axios";

const state: PodState = {
  availablesPods: {},
  userPods: {},
  podUsers: {}
};

const getters: GetterTree<PodState, null> = {
  getAvailablesPods(state) {
    return state.availablesPods;
  },
  getUserPods(state) {
    return state.userPods;
  },
  getPodUsers(state) {
    return state.podUsers;
  }
};

const mutations: MutationTree<PodState> = {
  GET_AVAILABLES_PODS: (state, pods) => {
    state.availablesPods = pods;
  },
  GET_USER_PODS: (state, pods) => {
    state.userPods = pods;
  },
  GET_POD_USERS: (state, users) => {
    state.podUsers = users;
  }
};

const actions: ActionTree<PodState, null> = {
  async addUserPod({ commit }, data) {
    try {
      await axios.post(
        "http://ec2-34-254-91-44.eu-west-1.compute.amazonaws.com:3000/api/pods",
        data
      );
      // if (res.status == (200 || 201)) {
      localStorage.setItem("waitingForNotif", String(true));
      localStorage.setItem("notif", "Pod créé avec succès !");
      // }
      commit;
    } catch (error) {
      console.log(error);
    }
  },
  async fetchAvailablesPods({ commit }, userId) {
    try {
      const { data } = await axios.get(
        `${"http://ec2-34-254-91-44.eu-west-1.compute.amazonaws.com:3000/api/pods/user/"}${userId}`
      );
      commit("GET_AVAILABLES_PODS", data);
    } catch (error) {
      console.log(error);
    }
  },
  async fetchUserPods({ commit }, userid) {
    try {
      const { data } = await axios.get(
        `${"http://ec2-34-254-91-44.eu-west-1.compute.amazonaws.com:3000/api/users/pods/"}${userid}`
      );
      commit("GET_USER_PODS", data);
    } catch (error) {
      console.log(error);
    }
  },
  async AddUserToPod({ commit }, data) {
    try {
      await axios.post(
        "http://ec2-34-254-91-44.eu-west-1.compute.amazonaws.com:3000/api/pods/user",
        data
      );
      // if (res.status == (200 || 201)) {
      localStorage.setItem("waitingForNotif", String(true));
      localStorage.setItem("notif", "Pod rejoint avec succès !");
      // }
      commit;
    } catch (error) {
      console.log(error);
    }
  },
  async modifUserPod({ commit }, data) {
    try {
      await axios.put(
        "http://ec2-34-254-91-44.eu-west-1.compute.amazonaws.com:3000/api/pods",
        data
      );
      commit;
    } catch (error) {
      console.log(error);
    }
  },
  async delUserPod({ commit }, podid) {
    try {
      await axios.delete(
        "http://ec2-34-254-91-44.eu-west-1.compute.amazonaws.com:3000/api/pods",
        {
          data: { id: podid }
        }
      );
      // if (res.status == (200 || 201)) {
      localStorage.setItem("waitingForNotif", String(true));
      localStorage.setItem("notif", "Pod supprimé avec succès !");
      // }
      commit;
    } catch (error) {
      console.log(error);
    }
  },
  async leaveUserPod({ commit }, data) {
    try {
      await axios.delete(
        "http://ec2-34-254-91-44.eu-west-1.compute.amazonaws.com:3000/api/pods/users",
        {
          data: { id: data.id, userUuid: data.userUuid }
        }
      );
      // if (res.status == (200 || 201)) {
      localStorage.setItem("waitingForNotif", String(true));
      localStorage.setItem("notif", "Pod quitté avec succès !");
      // }
      commit;
    } catch (error) {
      console.log(error);
    }
  },
  async ejectPodUser({ commit }, data) {
    try {
      await axios.delete(
        "http://ec2-34-254-91-44.eu-west-1.compute.amazonaws.com:3000/api/pods/users",
        {
          data: { id: data.id, userUuid: data.userUuid }
        }
      );
      // if (res.status == (200 || 201)) {
      localStorage.setItem("waitingForNotif", String(true));
      localStorage.setItem("notif", "Membre expulsé avec succès !");
      // }
      commit;
    } catch (error) {
      console.log(error);
    }
  },
  async fetchPodUsers({ commit }, podid) {
    try {
      const { data } = await axios.get(
        `${"http://ec2-34-254-91-44.eu-west-1.compute.amazonaws.com:3000/api/pods/users/"}${podid}`
      );
      commit("GET_POD_USERS", data);
    } catch (error) {
      console.log(error);
    }
  }
};

export const pods: Module<PodState, null> = {
  state,
  getters,
  mutations,
  actions
};
