import { Module } from "vuex";
import { PostState } from "@/store/modules/types";
import { GetterTree, MutationTree, ActionTree } from "vuex";
import axios from "axios";

const state: PostState = {
  userPosts: {},
  currentUserPost: {}
};

const getters: GetterTree<PostState, null> = {
  getUserPosts(state) {
    return state.userPosts;
  },
  getCurrentUserPost(state) {
    return state.currentUserPost;
  }
};

const mutations: MutationTree<PostState> = {
  GET_USER_POSTS: (state, posts) => {
    state.userPosts = posts;
  },
  GET_CURRENT_USER_POST: (state, post) => {
    state.currentUserPost = post;
  }
};

const actions: ActionTree<PostState, null> = {
  async addUserPost({ commit }, data) {
    try {
      await axios.post(
        "http://ec2-34-254-91-44.eu-west-1.compute.amazonaws.com:3000/api/posts",
        data
      );
      // if (res.status == (200 || 201)) {
      localStorage.setItem("waitingForNotif", String(true));
      localStorage.setItem("notif", "Post créé avec succès !");
      // }
      commit;
    } catch (error) {
      console.log(error);
    }
  },
  async addUserPostWithURL({ commit }, data) {
    try {
      await axios.post(
        "http://ec2-34-254-91-44.eu-west-1.compute.amazonaws.com:3000/api/posts/boost",
        data
      );
      // if (res.status == (200 || 201)) {
      localStorage.setItem("waitingForNotif", String(true));
      localStorage.setItem("notif", "Post créé avec succès !");
      // }
      commit;
    } catch (error) {
      console.log(error);
    }
  },
  async debitUserPost({ commit }, data) {
    try {
      const res = await axios.put(
        "http://ec2-34-254-91-44.eu-west-1.compute.amazonaws.com:3000/api/users/posts/remove",
        { id: data.userID }
      );
      console.log(res.status)
      commit;
    } catch (error) {
      console.log(error);
    }
  },
  async fetchUserPosts({ commit }, userId) {
    try {
      const { data } = await axios.get(
        `${"http://ec2-34-254-91-44.eu-west-1.compute.amazonaws.com:3000/api/posts/users/"}${userId}`
      );
      commit("GET_USER_POSTS", data);
    } catch (error) {
      console.log(error);
    }
  },
  async fetchCurrentUserPost({ commit }, postId) {
    try {
      const { data } = await axios.get(
        `${"http://ec2-34-254-91-44.eu-west-1.compute.amazonaws.com:3000/api/posts/"}${postId}`
      );
      commit("GET_CURRENT_USER_POST", data);
    } catch (error) {
      console.log(error);
    }
  },
  async modifUserPost({ commit }, data) {
    try {
      await axios.put(
        "http://ec2-34-254-91-44.eu-west-1.compute.amazonaws.com:3000/api/posts",
        data
      );
      // if (res.status == (200 || 201)) {
      localStorage.setItem("waitingForNotif", String(true));
      localStorage.setItem("notif", "Post modifié avec succès !");
      console.log(localStorage.getItem("notif"));
      // }
      commit;
    } catch (error) {
      console.log(error);
    }
  },
  async delUserPost({ commit }, data) {
    try {
      await axios.delete(
        "http://ec2-34-254-91-44.eu-west-1.compute.amazonaws.com:3000/api/posts",
        { data }
      );
      // if (res.status == (200 || 201)) {
      localStorage.setItem("waitingForNotif", String(true));
      localStorage.setItem("notif", "Post supprimé avec succès !");
      // }
      commit;
    } catch (error) {
      console.log(error);
    }
  },
  async creditUserPost({ commit }, data) {
    try {
      await axios.put(
        "http://ec2-34-254-91-44.eu-west-1.compute.amazonaws.com:3000/api/users/posts/add",
        { id: data.userUuid }
      );
      commit;
    } catch (error) {
      console.log(error);
    }
  }
};

export const posts: Module<PostState, null> = {
  state,
  getters,
  mutations,
  actions
};
