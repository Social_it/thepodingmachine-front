import { Module } from "vuex";
import { SubState } from "@/store/modules/types";
import { GetterTree, MutationTree, ActionTree } from "vuex";
import axios from "axios";

const state: SubState = {
  subscription: {},
  subUser: "",
  currentSub: "",
  currentTime: "",
  endSub: ""
};

const getters: GetterTree<SubState, null> = {
  getSubscriptions(state) {
    return state.subscription;
  }
};

const mutations: MutationTree<SubState> = {
  GET_SUBSCRIPTIONS: (state, subs) => {
    state.subscription = subs;
  }
};

const actions: ActionTree<SubState, null> = {
  async fetchSubscriptions({ commit }) {
    try {
      const { data } = await axios.get(
        "http://ec2-34-254-91-44.eu-west-1.compute.amazonaws.com:3000/api/subscription"
      );
      commit("GET_SUBSCRIPTIONS", data);
    } catch (error) {
      console.log(error);
    }
  },
  async addUserSubscription(
    { commit },
    data: {
      user: unknown;
      sub: unknown;
      startsub: unknown;
      endsub: unknown;
      postremaining: unknown;
    }
  ) {
    await axios.put(
      `${"http://ec2-34-254-91-44.eu-west-1.compute.amazonaws.com:3000/api/users/"}${
        data.user
      }${"/subscription/"}${data.sub}`,
      data.sub
    );
    await axios.put(
      `${"http://ec2-34-254-91-44.eu-west-1.compute.amazonaws.com:3000/api/users/"}${
        data.user
      }${"/startsub/"}${data.startsub}`,
      data.startsub
    );
    await axios.put(
      `${"http://ec2-34-254-91-44.eu-west-1.compute.amazonaws.com:3000/api/users/"}${
        data.user
      }${"/endsub/"}${data.endsub}`,
      data.endsub
    );
    commit;
  },
  async deleteUserSubscription(
    { commit },
    data: { user: unknown; sub: unknown }
  ) {
    await axios.put(
      `${"http://ec2-34-254-91-44.eu-west-1.compute.amazonaws.com:3000/api/users/"}${
        data.user
      }${"/subscription/"}${data.sub}`,
      data.sub
    );
    commit;
  }
};

export const subscriptions: Module<SubState, null> = {
  state,
  getters,
  mutations,
  actions
};
