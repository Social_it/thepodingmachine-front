import { Module } from "vuex";
import { CommentState } from "@/store/modules/types";
import { GetterTree, MutationTree, ActionTree } from "vuex";
import axios from "axios";

const state: CommentState = {
  listComments: {}
};

const getters: GetterTree<CommentState, null> = {
  getListComments(state) {
    return state.listComments;
  }
};

const mutations: MutationTree<CommentState> = {
  SET_COMMENTS: (state, comments) => {
    state.listComments = comments;
  }
};

const actions: ActionTree<CommentState, null> = {
  async createListComments({ commit }, data) {
    try {
      const res = await axios.post(
        "http://ec2-34-254-91-44.eu-west-1.compute.amazonaws.com:3000/api/comments",
        data
      );
      if (res.status == (200 || 201)) {
        localStorage.setItem("waitingForNotif", String(true));
        localStorage.setItem("notif", "Liste créée avec succès !");
      }
      commit;
    } catch (error) {
      console.log(error);
    }
  },
  async modifListComments({ commit }, data) {
    try {
      const res = await axios.put(
        "http://ec2-34-254-91-44.eu-west-1.compute.amazonaws.com:3000/api/comments/many/list",
        data
      );
      if (res.status == (200 || 201)) {
        localStorage.setItem("waitingForNotif", String(true));
        localStorage.setItem("notif", "Liste modifiée avec succès !");
      }
      commit;
    } catch (error) {
      console.log(error);
    }
  },
  async delListComment({ commit }, commentid) {
    try {
      await axios.delete(
        "http://ec2-34-254-91-44.eu-west-1.compute.amazonaws.com:3000/api/comments/deleteOneCommentInDatabase",
        { data: { id: commentid } }
      );
      commit;
    } catch (error) {
      console.log(error);
    }
  }
};

export const comments: Module<CommentState, null> = {
  state,
  getters,
  mutations,
  actions
};
