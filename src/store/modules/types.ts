export interface UserState {
  currentUserId: string | null;
  currentUser: {};
  allUsers: {};
}

export interface PostState {
  userPosts: {};
  currentUserPost: {};
}

export interface PodState {
  availablesPods: {};
  userPods: {};
  podUsers: {};
}

export interface ListState {
  commentsLists: {};
}
export interface CommentState {
  listComments: {};
}

export interface SubState {
  subscription: {};
  subUser: string;
  currentSub: string;
  currentTime: string;
  endSub: string;
}
