import { Module } from "vuex";
import { UserState } from "@/store/modules/types";
import { GetterTree, MutationTree, ActionTree } from "vuex";
import axios from "axios";

const state: UserState = {
  currentUserId: "",
  currentUser: {},
  allUsers: {}
};

const getters: GetterTree<UserState, null> = {
  getUserId(state) {
    return state.currentUserId;
  },
  getUserInfos(state) {
    return state.currentUser;
  },
  getAllUsers(state) {
    return state.allUsers;
  }
};

const mutations: MutationTree<UserState> = {
  INITIALISE_STORE: state => {
    const userId = localStorage.getItem("userId");
    state.currentUserId = userId;
  },
  SET_CURRENT_USER: (state, userData) => {
    state.currentUser = userData;
  },
  SET_ALL_USERS: (state, users) => {
    state.allUsers = users;
  }
};

const actions: ActionTree<UserState, null> = {
  async fetchCurrentUser({ commit }) {
    try {
      const user = state.currentUserId;
      const { data } = await axios.get(
        `${"http://ec2-34-254-91-44.eu-west-1.compute.amazonaws.com:3000/api/users/"}${user}`
      );
      commit("SET_CURRENT_USER", data);
    } catch (error) {
      console.log(error);
    }
  },
  async fetchAllUsers({ commit }) {
    try {
      const { data } = await axios.get(
        "http://ec2-34-254-91-44.eu-west-1.compute.amazonaws.com:3000/api/users"
      );
      commit("SET_ALL_USERS", data);
    } catch (error) {
      console.log(error);
    }
  }
};

export const user: Module<UserState, null> = {
  state,
  getters,
  mutations,
  actions
};
