import { Module } from "vuex";
import { ListState } from "@/store/modules/types";
import { GetterTree, MutationTree, ActionTree } from "vuex";
import axios from "axios";

const state: ListState = {
  commentsLists: {}
};

const getters: GetterTree<ListState, null> = {
  getUserLists(state) {
    return state.commentsLists;
  }
};

const mutations: MutationTree<ListState> = {
  SET_COMMENTSLIST: (state, listsData) => {
    state.commentsLists = listsData;
  }
};

const actions: ActionTree<ListState, null> = {
  async fetchCommentsLists({ commit }, userId) {
    try {
      const { data } = await axios.get(
        `${"http://ec2-34-254-91-44.eu-west-1.compute.amazonaws.com:3000/api/lists/user/"}${userId}`
      );
      commit("SET_COMMENTSLIST", data);
    } catch (error) {
      console.log(error);
    }
  },
  async modifUserList({ commit }, data) {
    try {
      await axios.put(
        "http://ec2-34-254-91-44.eu-west-1.compute.amazonaws.com:3000/api/lists",
        data
      );
      commit;
    } catch (error) {
      console.log(error);
    }
  },
  async delUserList({ commit }, listid) {
    try {
      const res = await axios.delete(
        "http://ec2-34-254-91-44.eu-west-1.compute.amazonaws.com:3000/api/lists",
        {
          data: { id: listid }
        }
      );
      if (res.status == (200 || 201)) {
        localStorage.setItem("waitingForNotif", String(true));
        localStorage.setItem("notif", "Liste supprimée avec succès !");
      }
      commit;
    } catch (error) {
      console.log(error);
    }
  }
};

export const lists: Module<ListState, null> = {
  state,
  getters,
  mutations,
  actions
};
