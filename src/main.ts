import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "@/store/index";
import VueI18n from "vue-i18n";
import i18n from "./i18n";
import Vuex from "vuex";
import moment from "moment";

Vue.use(Vuex);
Vue.use(VueI18n);
Vue.prototype.moment = moment;
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  i18n,
  beforeCreate() {
    this.$store.commit("INITIALISE_STORE");
  },
  render: h => h(App)
}).$mount("#app");
