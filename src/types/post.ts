export interface Post {
  id: number;
  content: string;
  images: string[];
  date: string;
  share: string;
  views: string;
  likes: string;
  comments: string;
  percents: string;
}
