export interface List {
  id: number;
  title: string;
  date: string;
  number: number;
  validate: true;
  usersId: string;
}
