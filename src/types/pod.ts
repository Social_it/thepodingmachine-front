export interface Pod {
  id: number;
  name: string;
  category: string;
  description: string;
  location: string;
  date: string;
  public: boolean;
  autoaccept: boolean;
  newusers: boolean;
  like: boolean;
  autocomment: boolean;
  manualcomment: boolean;
}
