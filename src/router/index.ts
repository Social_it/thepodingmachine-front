import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Cookies from "js-cookie";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "Home",
    component: () => import("../views/Home.vue"),
    meta: {
      requiresLogin: true,
      requiresAuth: false
    }
  },
  {
    path: "/posts",
    name: "Posts",
    component: () => import("../views/Posts.vue"),
    meta: {
      requiresAuth: false
    }
  },
  {
    path: "/post/:id",
    name: "Post",
    component: () => import("../views/Post.vue"),
    meta: {
      requiresAuth: true
    }
  },

  {
    path: "/pods",
    name: "Pods",
    component: () => import("../views/Pods.vue"),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/pod/:id",
    name: "Pod",
    component: () => import("../views/Pod.vue"),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/comments",
    name: "Comments",
    component: () => import("../views/Comments.vue"),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/comment/:id",
    name: "Comment",
    component: () => import("../views/Comment.vue"),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/boosts",
    name: "Boosts",
    component: () => import("../views/Boosts.vue"),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/activity",
    name: "Activity",
    component: () => import("../views/Activity.vue"),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/contact",
    name: "Contact",
    component: () => import("../views/Contact.vue"),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/about",
    name: "About",
    component: () => import("../views/About.vue"),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/admin",
    name: "Admin",
    component: () => import("../views/AdminDashboard.vue"),
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: "/admin/users",
    name: "AdminUsers",
    component: () => import("../views/AdminUsers.vue"),
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: "/admin/pods",
    name: "AdminPods",
    component: () => import("../views/AdminPods.vue"),
    meta: {
      requiresAuth: true,
    }
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  const user = localStorage.getItem("userId");
  if (to.matched.some(record => record.meta.requiresLogin)) {
    if (user) {
      next({ name: "Posts" });
    } else {
      next();
    }
  }

  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (user) {
      next();
      return;
    }
    next({ name: "Home" });
  } else {
    next();
  }
});

export default router;
